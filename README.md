Java web crawler app
====================
This is a basic Java web crawler app, built to explore wiprodigital.com
The application has few shortcomings due to time restriction (spent 2 hours).


Prerequisites
=============
You need the following tools to run the application:

* JDK 1.8

* Maven 3


Steps to run the app
====================
The application is maven based app. From the root folder please execute the following commands:

* "mvn clean install" -> this command to clear any existing target folder and generate a fresh binary.

* "mvn exec:java" -> this command to execute the main method which will trigger the crawler.

The output should be in a text file called "output.txt". The file is located under the root folder.

Shortcomings
============
* I couldn't follow the crawler's standard by checking robots.txt and follow the disallowed pathes as all images are stored under that disallowed path.
* Due to time restriction I couldn't write test cases.
* Used recursion which is not the best complexity!
* Did not specify a limit for the depth of pages to visit which can cause memory leak if the app got executed against huge website.
* Did not have time to fetch the images set by CSS (not direct img tag).
* The output is just a simple text file instead of sitemap generator - due to time restriction.
* Sometimes JSoup cannot connect to a specific URLs - not sure why as I needed time to investigate.