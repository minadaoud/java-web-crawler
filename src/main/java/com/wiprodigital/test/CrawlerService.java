package com.wiprodigital.test;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CrawlerService {
    private static final String A_HREF = "a[href]";
    private static final String HREF = "href";
    private static final String OUTPUT_FILE = "output.txt";

    String domain;
    //Using set to avoid duplicates
    private Set<String> pagesVisited = new HashSet<String>();
    private Set<String> staticResourcesResult = new HashSet<String>();

    //Wrapper method
    public void start(String url) throws Exception {
        domain = getDomainFromUrl(url);
        crawl(url);
        storeResult();
    }

    private void crawl(String url) {
        try {

            Document page = Jsoup.connect(url).get();
            Elements linksOnPage = page.select(A_HREF);
            //get static contents of this page before jump to next page
            getStaticContents(page);
            for (Element link : linksOnPage) {
                if (!pagesVisited.contains(link.absUrl(HREF))) {
                    if (link.attr(HREF).contains(domain) && !link.attr(HREF).contains("#") && !link.attr(HREF).contains(".pdf")) {
                        pagesVisited.add(link.absUrl(HREF));
                        crawl(link.absUrl(HREF));
                    }
                }
            }
        } catch (IOException ioException) {
            System.out.println("Could not connect to URL: " + url);
        }
    }

    //Here I am assuming that the images are the static contents we care about
    private void getStaticContents(Document page) {
        Elements staticResources = page.select("[src~=(?i)\\.(png|jpe?g|pdf)]");
        for (Element link : staticResources) {
            staticResourcesResult.add(link.absUrl("src"));
        }
    }

    private String getDomainFromUrl(String url) {
        Pattern pattern = Pattern.compile(".*?([^.]+\\.[^.]+)");
        String result = null;
        try {
            URI uri = new URI(url);

            Matcher m = pattern.matcher(uri.getHost());
            if (m.matches()) {
                result = m.group(1);
            }
        } catch (URISyntaxException uriException) {
            System.out.println("Could not get domain for URL: " + url);
        }
        return result;
    }

    private void storeResult() throws IOException {
        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream(OUTPUT_FILE));
            Iterator it = pagesVisited.iterator();
            Iterator it2 = staticResourcesResult.iterator();
            while (it.hasNext()) {
                out.println(it.next());
            }

            while (it2.hasNext()) {
                out.println(it2.next());
            }
        } finally {
            out.close();
        }
    }
}
